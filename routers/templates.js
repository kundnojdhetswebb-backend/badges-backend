const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const BadgeTemplate = require("../models/template")
const axios = require("axios");


router.get("/templates", async (req, res) => {
    try {
        const badges_template = await BadgeTemplate.findOne()
        res.json(badges_template)

    } catch (error) {
        res.send("Error " + err)
    }
});

router.post("/templates", async (req, res) => {
    const template = new BadgeTemplate({
        badge_collection: req.body.badge_collection,
        achieved_badges: req.body.achieved_badges
    })
    await template.save().then(r => res.json(r.data)).catch(err => res.json(err));
});

router.put("/template", async (req, res) => {

    const objectId = "5ff4399b30421b2784683d32";
    const b_collection = req.body.badge_collection;

    let document = {};
    await BadgeTemplate.findById(objectId).then(doc => document = doc).catch(err => console.log(err));


    b_collection.forEach(badge => document.badge_collection.push(badge));

    const update = {
        $set: {
            "badge_collection": document.badge_collection
        }
    };
    const options = { new: true };

    await BadgeTemplate.findByIdAndUpdate(objectId, update, options)
        .then(doc => res.json(doc)).catch(err => res.send(err));

});

router.put("/question", async (req, res) => {
    const category = req.body.category;
    const level = req.body.level;

    await BadgeTemplate.findOne({ "badge_collection.category": category, "badge_collection.level": level });
})

module.exports = router