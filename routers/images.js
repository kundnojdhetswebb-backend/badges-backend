// const router = express.Router();
// const path = require("path");

// router.get("/:imageName", (req, res) => {
//     const imageName = req.params.imageName;
//     res.sendFile(path.join(__dirname, '../images/', imageName));
// })

// module.exports = router;
const express = require('express')
const router = express.Router();
var path = require('path');
const fs = require('fs');

//param vad bilden heter, sen in i mongo som URL
router.get('/image/:badge', function (req, res) {

    image = req.params['badge'];
    var filename = path.resolve(__dirname + "/../images/" + image);

    res.sendFile(filename, function (err) {
        if (err) {
            res.status(err.status).end();
        } else {
            console.log("Sent: " + image)
        }
    })
});

// Get all badge-images from the image folder.
router.get('/images', (req, res) => {
    fs.readdir('./images', (err, files) => {
        res.send(files)
    })
});

module.exports = router
