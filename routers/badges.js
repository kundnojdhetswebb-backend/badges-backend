const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const Badge = require("../models/badge")
const axios = require("axios");
const BadgeTemplate = require("../models/template")

/**
 * API that is used during {@link delegate-backend/routers/login}. To chech whether a loggin in user already has badges connected. 
 */
router.get("/exists/:id", async (req, res) => {
    const isExisting = await Badge.exists({ "t_id": req.params.id });
    res.json({ data: isExisting })
})

/**
 * API that returns mongoDBs badges connected to a technician, based on technician id (t_id)
 */
router.get("/badges", async (req, res) => {
    await Badge.find()
    .then(r => res.send(r))
    .catch(err => {
        res.status(404).json({
            data: "Server ERROR",
        });
    });
});
/**
 * API that fetches a full stack of badges from a specific technician. If technician not found, send 404. 
 */
router.get("/badge/:id", async (req, res) => {
    const id = await req.params.id

    await Badge.findOne({ "t_id": id })
    .then((r) => res.json(r))
    .catch((err) => {
        res.status(500).json({
            data: "Server ERROR",
        });
    });
});

router.post("/badge", async (req, res) => {
    let template;
    await BadgeTemplate.findById()
    .then(r => template = r)
    .catch((err) => {
        res.status(500).json({
            data: "Server ERROR",
        });
    });
    
    const badge = new Badge({
        t_id: req.body.t_id,
        badge_collection: template.badge_collection,
        achieved_badges: template.achieved_badges
    })

    await badge.save()
    .then((res.status(201).json({ data: "Technician was loaded with template"})))
    .catch((err) => res.status(409).json({ data: "Could not save template to technician"}))
});

router.put("/badge", async (req, res) => {

    const category = req.body.category;
    const id = req.body.t_id;

    console.log(category)

    let data = {};
    const badge = await Badge.findOne({
        "t_id": id
        // "badge_collection.category": category
    });
    if (!badge)
        res.status(409).send("Couldt find any")

    console.log(badge);

    badge.badge_collection.forEach(c => {
        if (c.category === category) {
            c.current++;
            if (c.current >= c.goal) {
                badge.achieved_badges.push(c);
                badge.badge_collection.splice(badge.badge_collection.indexOf(c), 1);
            }
        }
    });
    // const b = badge.save();

    const find = {
        "t_id": id
    };

    const update = {
        $set: {
            "badge_collection": badge.badge_collection,
            "achieved_badges": badge.achieved_badges
        }
    };
    const options = { new: true };

    await Badge.findOneAndUpdate(find, update, options)
        .then(doc => res.json(doc)).catch(err => res.status(409).send(err));
    // res.json(b);
})



// router.get("/frontend", async (req, res) => {
//     console.log("IN FRONTEND");
//     await axios.get(
//         "http://localhost:8080/api/techniciansbyrealm/1",
//         {
//             params: {
//                 startDate: "2020-12-01",
//                 endDate: "2020-12-31"
//             }
//         }
//     ).then(r => res.json(r.data)).catch((err) => res.json(err));
// });


module.exports = router