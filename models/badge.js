const mongoose = require("mongoose");

const badgeSchema = new mongoose.Schema({
    t_id: Number,
    badge_collection: Array,
    achieved_badges: Array   
  
})

module.exports = mongoose.model("Badge",badgeSchema)