const mongoose = require("mongoose");

const badgeTemplate = new mongoose.Schema({
    badge_collection: Array,   
    achieved_badges: Array
})

module.exports = mongoose.model("BadgeTemplate",badgeTemplate)