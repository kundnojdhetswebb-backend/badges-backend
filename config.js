const dotenv = require('dotenv').config();

module.exports = {
    MONGO_USERNAME: process.env.MONGO_USERNAME || 'root',
    MONGO_PASSWORD: process.env.MONGO_PASSWORD || 'root',
    MONGO_PORT: process.env.MONGO_PORT || 27017,
    MONGO_DB: process.env.MONGO_DB || 'badges',
    MONGO_HOST: process.env.MONGO_HOST || '127.0.0.1',
    PORT: process.env.PORT || 3000
}