const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();
app.use(cors());

const config = require("./config");

const templatesRouter = require("./routers/templates");
const badgesRouter = require("./routers/badges");
const imagesRouter = require("./routers/images");

//console.log(`Username: ${config.MONGO_USERNAME}\nPassword: ${config.MONGO_PASSWORD}\nHost: ${config.MONGO_HOST}\nDB: ${config.MONGO_DB}`);
//const url = `mongodb://${config.MONGO_USERNAME}:${config.MONGO_PASSWORD}@${config.MONGO_HOST}:${config.MONGO_PORT}/${config.MONGO_DB}?authSource=admin`
const url = "mongodb+srv://admin:nEz9lzZs8YJtdrLu@badgescluster.3wm8e.mongodb.net/BadgesDB?retryWrites=true&w=majority"
mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
const con = mongoose.connection;

con.on("open", () => {
    console.log("connected :)")
})

app.use(express.json())

app.use("/api", badgesRouter);
app.use("/api", templatesRouter);
app.use("/api", imagesRouter);

app.listen(config.PORT, () => console.log(`Server is running on port: ${config.PORT}`));


